#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 25 21:47:47 2019

@author: gabriel
@visit: https://kapernikov.com/tutorial-image-classification-with-scikit-learn/
        https://arxiv.org/pdf/1706.02677.pdf
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
from dataToPickle import dataToPickle
from sklearn.model_selection import train_test_split
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import cross_val_predict
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from sklearn import svm

def unpickle(file):
  with open(file, 'rb') as fo:
    dict = pickle.load(fo, encoding = 'bytes')
    return dict

def inpickle(obj, name):
    with open(name, 'wb') as handle:
        pickle.dump(obj, handle, protocol=pickle.HIGHEST_PROTOCOL)

# Generate pickle
pkl = dataToPickle()
pkl.generatePickle()
# Reading dataset from .pkl generated in dataToPickle.py
data = unpickle('./dataset/ci_cert_form.pkl')
X = np.array(data['data'])
y = np.array(data['labels'])

# split data
X_train, X_test, y_train, y_test = train_test_split(
    X,
    y,
    test_size=0.4,
    shuffle=True,
    random_state=42,
)
scalify = StandardScaler()
X_train_prepared = scalify.fit_transform(X_train)

# Using SGD
sgd_clf = SGDClassifier(random_state=42, max_iter=1000, tol=1e-3)
sgd_clf.fit(X_train_prepared, y_train)

X_test_prepared = scalify.fit_transform(X_test)

# Pred with SGD
y_pred = sgd_clf.predict(X_test_prepared)

print(np.array(y_pred == y_test)[:25])
print('')
print('Percentage correct: ', 100*np.sum(y_pred == y_test)/len(y_test))

cmx = confusion_matrix(y_test, y_pred, labels=np.unique(y))
df = pd.DataFrame(cmx, columns=np.unique(y), index=np.unique(y))
df.columns.name = 'prediction'
df.index.name = 'label'
df
plt.imshow(cmx)
plt.xticks([])
plt.yticks([])
plt.colorbar()

inpickle(sgd_clf, 'sgdModel.pkl')


# Using SVM
svm_clf = svm.SVC(kernel='linear', random_state=0, gamma='auto', C=1)
svm_clf.fit(X_train_prepared, y_train)

# Pred with SVM
y_pred = svm_clf.predict(X_test_prepared)

print(np.array(y_pred == y_test)[:25])
print('')
print('Percentage correct: ', 100*np.sum(y_pred == y_test)/len(y_test))

cmx = confusion_matrix(y_test, y_pred, labels=np.unique(y))
df = pd.DataFrame(cmx, columns=np.unique(y), index=np.unique(y))
df.columns.name = 'prediction'
df.index.name = 'label'
df
plt.imshow(cmx)
plt.xticks([])
plt.yticks([])
plt.colorbar()

# Repo Clasification
from sklearn.metrics import classification_report
print(classification_report(y_test, y_pred))

inpickle(svm_clf, 'svmModel.pkl')
