#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 26 20:19:53 2019

@author: gabriel
"""

import numpy as np
import pickle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from dataToPickle import dataToPickle

def unpickle(file):
  with open(file, 'rb') as fo:
    dict = pickle.load(fo, encoding = 'bytes')
    return dict

def predict(files=[]):
    pkl = dataToPickle()
    X = pkl.getImageData(imagePath=files[0])
    for i in range(len(files)-1):
        imageData = pkl.getImageData(imagePath=files[i+1])
        X = np.concatenate((X, imageData), axis=0)
    scalify = StandardScaler()
    return scalify.fit_transform(X)
        
        
model = unpickle('./sgdModel.pkl')
X = predict(files=['test/form.jpg', 'test/cert.jpg', 'test/ci.jpg'])
y_pred = model.predict(X)
print(y_pred)
