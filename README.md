# trabajo_paper_modulo_2

Codigo para el trabajo del segundo modulo del diplomado Gabriel Casas Mamani.

Los modelos se encuentran serializados en los archivos **sgdModel.pkl** y **svmModel.pkl**

el archivo que los ejecuta es: **svm_sgd.py**
para serializar el dataset se tiene archivo **dataToPickle.py**
el data set serializado se emplea en el archivo **fitModel.py** para entrenar los modelos.


## Instalar 
Se tiene que tener instalado Python3 y pip3
```
$ pip3 install numpy
$ pip3 install pickle-mixin
$ pip3 install sklearn
$ pip3 install matplotlib
$ pip3 install opencv-python
$ pip3 install pandas
```
## Ejecucion

Copiar los archivos .sample de la carpeta test quitando la extencion .sample

solo se debe ejecutar el archivo **svm_sgd.py** este devolvera un array con los nombres que el modelo clasifico para cada imagen definida en el archivo
```
$ python3 svm_sgd.py
['formulario' 'certificado' 'ci']
```

para probar otros documentos modificar el archivo **svm_sgd.py** en la linea 33 y agregar al array **mas de dos direcciones de documentos por la serializacion**

```python
X = predict(files=['test/form.jpg', 'test/cert.jpg', 'test/ci.jpg'])
```

Adicional

El data set esta serializado en archivo **ci_cert_form.pkl** y los modelos entrenados en los archivo **sgdModel.pkl** para SGD y **svmModel.pkl** para SVM