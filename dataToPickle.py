#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 26 08:48:20 2019

@author: gcasas
"""
import numpy as np
import cv2
import os
import pickle

class dataToPickle:

    def __init__(self):
        self.dataset = None
        self.imageSize = 100
        
    def getDataset(self, originPath, example = 20):
        if (os.path.exists(originPath)):
            dataset = np.zeros((example, self.imageSize * self.imageSize))
            self.nameFile = []
            i = 0
            for imagePath in os.listdir(originPath):
                fullPatImage = "%s/%s" % (originPath, imagePath)
                if (fullPatImage.find('jpg') > 0 or fullPatImage.find('JPG') >0):
                    image = cv2.imread(fullPatImage)
                    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    resizedImage = cv2.resize(gray, (self.imageSize, self.imageSize))
                    dataset[i, :] = resizedImage.flatten()[np.newaxis]
                    self.nameFile.append(imagePath)
                    i += 1
                else:
                    print("No se encontraron imagens .jpg o .JPG revise el directorio")
                    return None
            return dataset
        else:
            print("%s no es un directorio"%(originPath))
            return None

    def getImageData(self, imagePath):
        if (os.path.exists(imagePath)):
            imageData = np.zeros((1, self.imageSize * self.imageSize))
            self.nameFile = []
            if (imagePath.find('jpg') > 0 or imagePath.find('JPG') > 0):
                image = cv2.imread(imagePath)
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                resizedImage = cv2.resize(gray, (self.imageSize, self.imageSize))
                imageData[0, :] = resizedImage.flatten()[np.newaxis]
                self.nameFile = imagePath
            else:
                print("No se encontraron imagens .jpg o .JPG revise el directorio")
                return None
            return imageData
        else:
            print("%s no es un directorio"%(imagePath))
            return None
    def generatePickle(self):
        data = np.array([])
        filename = np.array([])
        label = np.array([])
        
        
        pkl = dataToPickle()
        
        # ==== CI
        i_data = pkl.getDataset(originPath='../dataset/ci', example = 112)
        i_filename = np.array(pkl.nameFile)
        i_label = np.repeat('ci',len(i_filename))
        
        data = i_data
        filename = i_filename
        label = i_label
        
        # ===== Certificado
        i_data = pkl.getDataset(originPath='../dataset/certificado', example = 72)
        i_filename = np.array(pkl.nameFile)
        i_label = np.repeat('certificado',len(i_filename))
        
        data = np.concatenate((data, i_data), axis=0)
        filename = np.concatenate((filename, i_filename), axis=0)
        label = np.concatenate((label, i_label), axis=0)
        
        # ===== Formulario
        i_data = pkl.getDataset(originPath='../dataset/formularios', example = 142)
        i_filename = np.array(pkl.nameFile)
        i_label = np.repeat('formulario',len(i_filename))
        
        data = np.concatenate((data, i_data), axis=0)
        filename = np.concatenate((filename, i_filename), axis=0)
        label = np.concatenate((label, i_label), axis=0)
        
        
        data_pkl = {'data': data, 'file_names': filename, 'labels': label}
        
        with open('ci_cert_form.pkl', 'wb') as handle:
            pickle.dump(data_pkl, handle, protocol=pickle.HIGHEST_PROTOCOL)
